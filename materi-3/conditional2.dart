void main() {
  var buttonPushed = 3;
  switch (buttonPushed) {
    case 1 :
      {
        print(
            'Senin : Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
        break;
      }
    case 2:
      {
        print(
            'Selasa : Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
        break;
      }
    case 3:
      {
        print(
            'Rabu : Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
        break;
      }
    case 4:
      {
        print(
            'Kamis : Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
        break;
      }
    case 5:
      {
        print(' Jumat : Hidup tak selamanya tentang pacar.');
        break;
      }
    case 6:
      {
        print('Sabtu : Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
        break;
      }
    case 7:
      {
        print(
            'Minggu : Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
        break;
      }
    default:
      {
        print('Tidak terjadi apa-apa');
      }
  }
}
