// void main(List<String> args) {
//   var angka = 100;
//   print(angka == 100);
//   print(angka == 20);
// }

// void main(List<String> args) {
//   var sifat = "rajin";
//   print(sifat != "malas"); // true
//   print(sifat != "bandel"); //true
// }

// void main(List<String> args) {
//   var angka = 8;
//   print(angka == "8");
//   print(angka == "8");
//   print(angka == 8);
// }

// void main(List<String> args) {
//   var angka = 11;
//   print(angka != "11"); // false, padahal "11" adalah string
//   print(angka != "11"); // true, karena tipe datanya berbeda
//   print(angka != 11); // false
// }

// void main(List<String> args) {
//   var number = 17;
//   print(number < 20); // true
//   print(number > 17); // false
//   print(number >= 17); // true, karena terdapat sama dengan
//   print(number <= 20); // true
// }

// void main(List<String> args) {
//   print(true || true); // true
//   print(true || false); // true
//   print(true || false || false); // true
//   print(false || false); // false
// }

// void main(List<String> args) {
//   print(true && true); // true
//   print(true && false); // false
//   print(false && false); // false
//   print(false && true && true); // false
//   print(true && true && true); // true
// }

void main(List<String> args) {
  var word = "Teguh dwi pramono";
  print(word[0]);
  print(word[8]);
}


// void main(List<String> args) {
//   print(num.parse("123"));
//   print(num.parse("334"));
// }

// void main(List<String> args) {
//   int j = 45;
//   String t = "$j";
//   print("hello" + t);
// }

// void main(List<String> args) {
//   final umur = 21;

//   // error: 'umur', a final variable, can only be set once
//   const age = 22;
//   // age = 22;
//   print(umur);
//   //error: Constant variabels can't be assigned a value
// }
